import time
import requests as rq
from turtle import goto
from selenium import webdriver
import selenium
from selenium.webdriver.common.keys import Keys
# 等待整个table加载完成，否则因网络延迟，driver.page_source可能会拿不到数据 
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
import config
#1.创建Edge浏览器对象，这会在电脑上在打开一个浏览器窗口
import json
# options1 = EdgeOptions()
# options1.use_chromium = True
# options1.add_argument("--ignore-certificate-errors")
# options1.add_argument("ignore-certificate-errors")
# options1.add_argument("--ignore-ssl-errors")
# options1.add_argument("-ignore-certificate-errors")
# options1.add_argument("-ignore -ssl-errors")
# driver = Edge(options=options1)
driver = webdriver.Chrome()
MODE_OVERRIDE = 0
MODE_PASS = 1 # 没实现

#  /html/body/div/div/div[contains(@class, 'ant-select')]/div/ul/li[2]

# 参数：
DATA_CACHE_JSON_FILE = r"./a.json"
LOG_JSON_FILE = r"./err.json"

SCHOOL_ID_SOURCE_FILE = r"./idlist.csv"
# SCHOOL_ID_SOURCE_FILE = r"D:\sssss\a666\爬虫整理\sele\testId.txt"
ARG_PROVINCE_STR = "湖北"
ARG_YEAR_LIST = [2021]
ARG_MODE = MODE_OVERRIDE
ARG_READCACHE = True

# SHOTCUT_ROOT = r"D:\Apachong\screenshot"

def wait():
    WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, "/html"))) 
    time.sleep(0.4)
    
def findData():
    thisDataList = []
    haveNext = True
    while haveNext:
        #找到表格
        # ele = driver.find_element_by_xpath("/html/body/div[1]/div/div[1]/div/div/div/div[1]/div[3]/div[1]/div[1]/div/div[1]/div[2]/div[1]/table/tbody")
        ele = driver.find_element_by_xpath("/html//table[1]/tbody")
        lines = ele.find_elements_by_tag_name("tr")
        for line in lines:
            thisDataList.append(line.text)
            # print(line.text)
        haveNext = False
        # 找到下一页按钮
        # try:
        #     ele = driver.find_element_by_xpath("/html/body/div[1]/div/div[1]/div/div/div/div[1]/div[3]/div[1]/div[1]/div/div[1]/div[2]/div[3]/div/div/ul/li[4]")
        # except:
        #     break
        # haveNextBoolStr = ele.get_attribute("aria-disabled")
        # if (haveNextBoolStr=="false"):
        #     haveNext = False
        # elif (haveNextBoolStr=="true"):
        #     ele.click()
        #     wait()
    return thisDataList

def save():
    with open(DATA_CACHE_JSON_FILE,mode="w", encoding="utf8") as f:
        json.dump(data,f)
        print("加载入文件完成...")
    log = {}
    log["errid"] = errId
    with open(LOG_JSON_FILE,mode="w", encoding="utf8") as f:
        json.dump(log,f)
        print("加载入文件完成...")    


def oneSchoolp(schoolId):
    schoolId = str(schoolId)
    print(schoolId)
    driver.get(f'https://gkcx.eol.cn/school/{str(schoolId)}/provinceline')
    wait()
    thisDataDict = {}
    # 点击省份
    ele = driver.find_elements_by_class_name("dropdown-box")[0]
    ele.click()
    ele.click()
    # wait()
    # try:
    #     print("=================================================================find sheng")
    #     driver.find_element_by_xpath(f"/html/body/div[3]/div/div/div/ul/li[contains(text(),'{ARG_PROVINCE_STR}')]")
    # except Exception:
    #     print("没找到省份")
    #     return thisDataDict
    # print("=================================================================middle")
    # print(ARG_YEAR_LIST)
    mzd = False
    for year in ARG_YEAR_LIST:
        print("0000000000000000000000000000000000000year")
        print("=================================================================year"+str(year))
        # 找年份
        driver.find_elements_by_class_name("dropdown-box")[1].click()
        wait()
        try:
            driver.find_element_by_xpath(f"/html/body//div/div/div/ul/li[contains(text(),'{str(year)}')]").click()
            print("zhaowanle")
            wait()
        except Exception:
            print("没找到")
            driver.refresh()
            wait()
            mzd = True
            continue
        # 找所有的
        print("=================================================================to find wenli")
        num = 1
        while True:
            ele = driver.find_elements_by_class_name("dropdown-box")[2]
            ele.click()
            # wenliStr = f"/html/body/div/div/div[contains(@class, 'ant-select')]/div/ul/li[{str(num)}]"
            # wenliStr = f"/html/body//div[contains(@style, 'position: absolute')][3]/div/div/div/ul/li[{str(num)}]"
            wenliStr = f"/html/body/div[4]/div/div/div/ul/li[{str(num)}]"
            wenliStr = f"/html/body/div/div/div[1]/div/div/div[1]/div[3]/div[1]/div[1]/div/div[1]/div[1]/div/div[3]/div[2]/div/div/div/ul/li[{str(num)}]"
            wait()
            try:
                # hhhh
                print("=================================================================wenli count", num)

                ele = driver.find_element_by_xpath(wenliStr)
                # temp = ele.find_element_by_xpath("div/ul/li[1]")
            except Exception as e:
                print(e)
                print("=================================================================except")
                break
            print("=================================================================before click")
            ele = driver.find_element_by_xpath(wenliStr)
            wenliName = driver.execute_script("return $(arguments[0])[0].innerText",ele)
            print("=================================================================find it"+f"/html/body/div/div/div[contains(@class, 'ant-select')]/div/ul/li[{str(num)}]")
            driver.execute_script("$(arguments[0]).click()",ele)
            print("=================================================================after click")
            wait()
            if ARG_MODE==MODE_OVERRIDE:
                thisDataDict[f"{str(year)}|{wenliName}"] = findData()
                print("=================================================================")
                print(thisDataDict[f"{str(year)}|{wenliName}"])
                print(f"{str(year)}|{wenliName}|")
                print("=================================================================")
            else:
                print("还没写=================================================================")
            # if (ele.text!=""):
            num += 1
    # if mzd:
    #     driver.execute_script("window.scrollBy(0,300)")
    #     driver.get_screenshot_as_file(SHOTCUT_ROOT+"/"+schoolId+".png")
    return thisDataDict


def logIn():
    driver.get(f'https://gkcx.eol.cn/school/443/provinceline')
    input("登录后点击回车")
    # dictCookies=driver.get_cookies()
    # return dictCookies


# document.getElementsByClassName("ant-select-selection-selected-value")
# document.getElementsByClassName("dropdown-box")


ck = logIn()
if ARG_READCACHE:
    with open(DATA_CACHE_JSON_FILE,'r',encoding="utf8") as load_f:
        load_dict = json.load(load_f)
        print(load_dict)
    data = load_dict
else:
    data = {}

with open(SCHOOL_ID_SOURCE_FILE) as fp:
    ls = fp.readlines()
    ls = [i[:-1] for i in ls]

errId = []
counter = 0
for schoolId in ls:
    counter += 1
    try:
        pDict = oneSchoolp(schoolId=schoolId)
        data[schoolId] = pDict
    except Exception as e:
        errId.append(schoolId)
        
        if str(e)!="list index out of range":
            rq.get("http://server.acbs.top:8081/cursettrue/")
            input("")
        print("外部报错", e)
    if counter%10==2:
        print("ssssssssssssssssssssssave")
        save()
    rq.post(config.SERVER_PREFIX+"/ok",{"name":config.NAME, "id":str(schoolId)})
save()