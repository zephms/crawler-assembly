
# python command: 所用的python在环境变量中的名字,如python,python3等
PYTHON = "python"

# mode 爬取模式,可选值为2和3
MODE=2

# name idlist的名字,根据名字从服务器拉取要爬取的id
NAME="testid"

# override server 无视服务器进度启动,不清楚请用False
OVERRIDE_SERVER = False

# server
SERVER_PREFIX = "http://server.acbs.top:99"